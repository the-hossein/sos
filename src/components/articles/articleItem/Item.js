import Image from "next/image";
import Link from "next/link";
import React from "react";
import Style from "./ArticleItem.module.css";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
const ArticleItem = (props) => {
  return (
    <div className={Style.article_card}>
      <div>
        <Image src={props.pic} alt="Article" width={420} height={250} />
      </div>
      <div>
        <h3>{props.title}</h3>
        <h6>
          <AccessTimeIcon />
          {props.duration}
        </h6>
        <p>{props.description}</p>
        <div>
          <Link href={props.link}>ادامه</Link>
        </div>
      </div>
    </div>
  );
};

export default ArticleItem;

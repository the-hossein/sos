import { Grid } from "@mui/material";
import React from "react";
import { TitrRow } from "../../tools/titrRow/TitrRow";
import ArticleItem from "./articleItem/Item";

const Articles = (props) => {
  return (
    <section className="sos-container">
      <TitrRow textLink="نمایش همه" titr="مقاله ها" link="#" />

      <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        {props.Items.map((Item) => {
          return (
            <Grid item key={Item.id} lg={3} sm={6} xs={12}>
              <ArticleItem
                pic={Item.pic}
                title={Item.title}
                description={Item.description}
                duration={Item.duration}
                link={Item.link}
              />
            </Grid>
          );
        })}
      </Grid>
    </section>
  );
};

export default Articles;

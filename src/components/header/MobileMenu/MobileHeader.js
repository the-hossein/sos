import Image from "next/image";
import Link from "next/link";
import React, { useState } from "react";
import Style from "./MobileHeader.module.css";
import MenuIcon from "@mui/icons-material/Menu";
import Logo from "../../../../public/assets/logo.png";
import UserAvatar from "../../../../public/assets/user.jpeg";
import CloseIcon from "@mui/icons-material/Close";
const MobileHeader = () => {
  const [ManuState, setManuState] = useState(0);
  const ChangeMenuState = (state) => {
    setManuState(state);
  };
  return (
    <header className={Style.m_header}>
      <nav
        className={
          ManuState === 1
            ? Style.m_menu
            : `${Style.m_menu} ${Style.m_menu_close}`
        }
      >
        <CloseIcon onClick={() => ChangeMenuState(0)} />
        <ul>
          <li>
            <Link href="#">صفحه اصلی</Link>
          </li>
          <li>
            <Link href="#">تودولیست</Link>
          </li>
        </ul>
      </nav>
      <div className={Style.menu_icon}>
        <MenuIcon onClick={() => ChangeMenuState(1)} />
      </div>
      <div className={Style.logo}>
        <Image src={Logo} />
      </div>
      <div className={Style.user_avatar}>
        <Image src={UserAvatar} />
      </div>
    </header>
  );
};

export default MobileHeader;

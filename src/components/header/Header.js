import React, { useLayoutEffect, useState } from "react";
import DesktopHeader from "./DesktopMenu/DesktopHeader";
import MobileHeader from "./MobileMenu/MobileHeader";

const Header = () => {
  const [size, setSize] = useState([0]);
  useLayoutEffect(() => {
    function updateSize() {
      setSize([window.innerWidth]);
    }
    window.addEventListener("resize", updateSize);
    updateSize();
    return () => window.removeEventListener("resize", updateSize);
  }, []);

  return <>{size > 678 ? <DesktopHeader /> : <MobileHeader/>}</>;
};

export default Header;

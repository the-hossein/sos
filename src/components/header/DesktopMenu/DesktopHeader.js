import React from "react";
import Style from "./DesktopHeader.module.css";
import Logo from "../../../../public/assets/logo.png";
import Image from "next/image";
import Link from "next/link";
import UserAvatar from "../../../../public/assets/user.jpeg"
const DesktopHeader = () => {
  return (
    <header className={Style.d_header}>
      <div className={Style.logo}>
        <Image src={Logo} />
      </div>
      <nav className={Style.d_menu}>
        <ul>
          <li>
            <Link href="#">صفحه اصلی</Link>
          </li>
          <li>
            <Link href="#">تودولیست</Link>
          </li>
        </ul>
      </nav>
      <div className={Style.user_avatar}>
        <Image src={UserAvatar}/>
      </div>
    </header>
  );
};

export default DesktopHeader;

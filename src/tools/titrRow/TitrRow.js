import Link from "next/link";
import React from "react";
/* import { TitrWithLink } from "./StyleTitrRow"; */
import Style from './TitrRow.module.css'
export const TitrRow = (props) => {
  return (
    <>
      <div className={Style.titr_with_link}>
        <h2>{props.titr}</h2>
        <Link href={props.link}>{props.textLink}</Link>
      </div>
    </>
  );
};
